export function kirkmanPairing(n: number, rd: number): number[][] {

    let addNum = n
    if (n % 2 !== 0) {
        n = n + 1;
    }
    
    let idex: [number, number] = [rd, rd];
    const pair: number[][] = [];

    for (let i = 0; i < n / 2; i++) {

        if (idex[0] === idex[1] && idex[0] <= addNum && n <= addNum && idex[0] != n) {
            pair.push([idex[0], n]);
        } else if (idex[0] !== idex[1] && idex[0] <= addNum && idex[1] <= addNum) {
            pair.push(idex);
        }

        idex = [idex[0] - 1, idex[1] + 1];

        if (idex[0] < 1) {
            idex[0] = n - 1;
        }
        if (idex[1] > n - 1) {
            idex[1] = 1;
        }
    }

    return pair;
}