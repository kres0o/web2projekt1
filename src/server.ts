import express from 'express';
import fs from 'fs';
import path from 'path'
import https from 'https';
import { auth, requiresAuth} from 'express-openid-connect'; 
import dotenv from 'dotenv'
import {client, createTables, getCijeloN, getKolo, getMaxId, getNatjecanje, getNatjecatelji, getNatjecateljiID, getNatjecateljiRows, getUsers, insertKolo, insertNatjecanje, insertNatjecatelji, insertUsers, updateKolo, updateNatjecatelja} from './database.js';
import { kirkmanPairing } from './kirkmanovAlg';

dotenv.config()

createTables()

const app = express();
app.set("views", path.join(__dirname, "views"));
app.set('view engine', 'pug');

const externalUrl = process.env.RENDER_EXTERNAL_URL;
const port = externalUrl && process.env.PORT ? parseInt(process.env.PORT) : 4080;

const config = { 
  authRequired : false,
  idpLogout : true, //login not only from the app, but also from identity provider
  secret: process.env.SECRET,
  baseURL: externalUrl || `https://localhost:${port}`,
  clientID: process.env.CLIENT_ID,
  issuerBaseURL: 'https://dev-bp6j8i7ix5fc61qj.us.auth0.com',
  clientSecret: process.env.CLIENT_SECRET,
  authorizationParams: {
    response_type: 'code' ,
    //scope: "openid profile email"   
   },
};

app.use(express.urlencoded({ extended: true }));
// auth router attaches /login, /logout, and /callback routes to the baseURL
app.use(auth(config));

app.get('/',  function (req, res) {
  let username : string | undefined;
  if (req.oidc.isAuthenticated()) {
    username = req.oidc.user?.nickname ?? req.oidc.user?.sub;
  }
  res.render('index', {username});
});

app.get('/natjecanje', requiresAuth(), async function (req, res) {       
  const user = req.oidc.user;
  var natjecanja
  if (user && user.name) {
    natjecanja = (await getNatjecanje(user.name)).rows  
  }
  res.render('natjecanje', {user, natjecanja}); 
});

/*app.post('/natjecanje', requiresAuth(), function (req, res) {
  res.redirect(`/natjecanje`)
});*/

app.get("/sign-up", (req, res) => {
  res.oidc.login({
    returnTo: '/natjecanje',
    authorizationParams: {      
      screen_hint: "signup",
    },
  });
});

app.get('/natjecanje/unos-podataka/:id', async function (req, res) {       
  const user = req.oidc.user
  const id = req.params.id  
  const podatci = (await getCijeloN(id)).rows 
  const natjecatelji = (await getNatjecatelji(id)).rows
  var link = config.baseURL + `/natjecanje/unos-podataka/${id}`
  res.render('unosPodataka', {user, id, podatci, natjecatelji, link}); 
});

app.post('/natjecanje/unos-podataka/:id', async function (req, res) {
  const id = req.params.id 
  const kola = (await getCijeloN(id)).rows
  const nat = (await getNatjecatelji(id)).rows
  for (let i = 0; i < nat.length; i++) {
    await updateNatjecatelja(0, nat[i].natjecateljid)
  }
  for (let i = 0; i < kola.length; i++) {
    let k = `kolo ${kola[i].brojk}`
    let rez = req.body[k][i % req.body[k].length]
    await updateKolo(rez ,kola[i].koloid)
    let oldB1 = (await getNatjecateljiID(kola[i].natjecatelj1)).bodovi
    let newB1
    if (rez == 1) {
      newB1 = Number(oldB1) + Number(kola[i].pobjeda)
    } else if (rez == 2) {
      newB1 = Number(oldB1) + Number(kola[i].poraz)
    } else if (rez == -1) {
      newB1 = Number(oldB1) + Number(kola[i].remi)
    } else if (rez == 0) {
      newB1 = Number(oldB1)
    }
    await updateNatjecatelja(newB1, kola[i].natjecatelj1)
    let oldB2 = (await getNatjecateljiID(kola[i].natjecatelj2)).bodovi
    let newB2
    if (rez == 2) {
      newB2 = Number(oldB2) + Number(kola[i].pobjeda)
    } else if (rez == 1) {
      newB2 = Number(oldB2) + Number(kola[i].poraz)
    } else if (rez == -1) {
      newB2 = Number(oldB2) + Number(kola[i].remi)
    } else if (rez == 0) {
      newB2 = Number(oldB2)
    }
    await updateNatjecatelja(newB2, kola[i].natjecatelj2)
  }
  res.redirect(`/natjecanje/unos-podataka/${id}`)
})

app.post("/natjecanje/unos-podataka", requiresAuth(), async (req, res) => {
  const user = req.oidc.user
  if (user && user.name) {
    const rows = await getUsers(user.name)
    if (rows == 0) {
      await insertUsers(user.name)
    }
  }
  var idNatjecanja
  if (user && user.name) {
    const sustavB = req.body.sustavB.split('/')
    const brojN = req.body.brojN.split((/[;\n]/))
    const brojNT = brojN.map((x: string) => x.trim())
    const natjecatelji = brojNT.filter((str: string) => str.trim() !== "");
    await insertNatjecanje(req.body.natjecanjeN, user.name, sustavB[0], sustavB[1], sustavB[2])
    idNatjecanja = await getMaxId()
    const listN = []
    for (let i = 0; i < natjecatelji.length; i++) {
      await insertNatjecatelji(natjecatelji[i], idNatjecanja, 0)
      listN.push(await getNatjecateljiRows())
    }
    var numKola = natjecatelji.length
    if (natjecatelji.length % 2 == 0) {
      numKola = numKola - 1
    }

    for (let i = 0; i < numKola; i++) {
      const raspored = kirkmanPairing(natjecatelji.length, i + 1)
      for (let j = 0; j < raspored.length; j++) {
        await insertKolo(idNatjecanja, i + 1, listN[raspored[j][0] - 1], listN[raspored[j][1] - 1], 0)
      }
    }
  }
  res.redirect(`/natjecanje/unos-podataka/${idNatjecanja}`)
})

if (externalUrl) {
  const hostname = '0.0.0.0'; //ne 127.0.0.1
  app.listen(port, hostname, () => {
  console.log(`Server locally running at http://${hostname}:${port}/ and from
  outside on ${externalUrl}`);
  });
} 
else {
  https.createServer({
  key: fs.readFileSync('server.key'),
  cert: fs.readFileSync('server.cert')
  }, app)
  .listen(port, function () {
  console.log(`Server running at https://localhost:${port}/`);
  });
}
