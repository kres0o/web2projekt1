function isNumeric(str) {
    if (typeof str != "string") return false
    return !isNaN(str) && !isNaN(parseFloat(str))
  }

function validate(input) {
    const natjecatelji1 = input.brojN.value.split(/[;\n]/)
    const natjecatelji = natjecatelji1.filter((str) => str.trim() !== "");
    if (natjecatelji.length < 4) {
        alert('Minimalno treba biti 4 natjecatelja')
        return false
    } else if (natjecatelji.length > 8) {
        alert('Maksimalno treba biti 8 natjecatelja')
        return false
    }

    const bod = input.sustavB.value.split('/')
    if (bod.length != 3) {
        alert('Oblik mora biti pobjeda/remi/poraz (float/float/float)')
        return false
    } else {
        let num = false
        for (let i = 0; i < bod.length; i++) {
            if (!isNumeric(bod[i])) {
                console.log(bod[i])
                num = true
            }
        }
        if (num) {
            alert('Oblik mora biti pobjeda/remi/poraz (float/float/float)')
            return false
        } 
    }

    if (input.natjecanjeN.value.length == 0) {
        alert('Mora postojati neko ime')
        return false
    } 

    return true
}