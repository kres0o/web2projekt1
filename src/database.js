const {Client} = require('pg')

export const client = new Client({
    host:process.env.DBHOST,
    user:process.env.DBUSER,
    port:process.env.DBPORT,
    password:process.env.DBPASSWORD,
    database:process.env.DBDATABASE
})
client.connect()

export const createTables = () => {
    client.query('CREATE TABLE if not exists users (userID SERIAL, userName varchar(255))')
    client.query('CREATE TABLE if not exists natjecanje (natjecanjeID SERIAL, natjecanjeName varchar(255), userName varchar(255), pobjeda DECIMAL, remi DECIMAL, poraz DECIMAL)')
    client.query('CREATE TABLE if not exists kolo (koloID SERIAL, natjecanjeID INT, brojK INT, natjecatelj1 int, natjecatelj2 int, pobjednik int)')
    client.query('CREATE TABLE if not exists natjecatelji (natjecateljID SERIAL, natjecateljName varchar(255), natjecanjeID INT, bodovi DECIMAL)')
} 

export const getUsers = async (name) => {
    const data = await client.query('SELECT * FROM users WHERE username = $1', [name]);
    return data.rows.length;
};

export const insertUsers = async (name) => {
    await client.query(`INSERT INTO "users" ("username") VALUES ($1)`, [name])
}

export const insertNatjecanje = async (natjecanjeName, userName, pobjeda, remi, poraz) => {
    await client.query(`INSERT INTO "natjecanje" ("natjecanjename", "username", "pobjeda", "remi", "poraz") VALUES ($1, $2, $3, $4, $5)`, [natjecanjeName, userName, pobjeda, remi, poraz])
}

export const getMaxId = async () => {
    const data = await client.query('SELECT MAX(natjecanjeid) as maks FROM natjecanje');
    return data.rows[0].maks;
};

export const insertNatjecatelji = async (natjecateljname, natjecanjeid, bodovi) => {
    await client.query(`INSERT INTO "natjecatelji" ("natjecateljname", "natjecanjeid", "bodovi") VALUES ($1, $2, $3)`, [natjecateljname, natjecanjeid, bodovi])
}

export const getNatjecateljiRows = async () => {
    const data = await client.query('SELECT * FROM natjecatelji');
    return data.rows.length;
};

export const insertKolo = async (natjecanjeid, brojk, natjecatelj1, natjecatelj2, pobjednik) => {
    await client.query(`INSERT INTO "kolo" ("natjecanjeid", "brojk", "natjecatelj1", "natjecatelj2", "pobjednik") VALUES ($1, $2, $3, $4, $5)`, [natjecanjeid, brojk, natjecatelj1, natjecatelj2, pobjednik])
}

export const getNatjecanje = async (name) => {
    const data = await client.query('SELECT * FROM natjecanje WHERE username = $1', [name]);
    return data;
};

export const getCijeloN = async (id) => {
    const data = await client.query(`select *, (select natjecateljname as nat1
        from natjecatelji
        where natjecanje.natjecanjeid = natjecanjeid
        and kolo.natjecatelj1 = natjecateljid),
        (select natjecateljname as nat2
        from natjecatelji
        where natjecanje.natjecanjeid = natjecanjeid
        and kolo.natjecatelj2 = natjecateljid),
        (select bodovi as bod1
        from natjecatelji
        where natjecanje.natjecanjeid = natjecanjeid
        and kolo.natjecatelj1 = natjecateljid),
        (select bodovi as bod2
        from natjecatelji
        where natjecanje.natjecanjeid = natjecanjeid
        and kolo.natjecatelj2 = natjecateljid)
from natjecanje natural join kolo
where natjecanjeid = $1
order by brojk`, [id]);
    return data;
};

export const getKolo = async (id) => {
    const data = await client.query('SELECT * FROM kolo WHERE natjecanjeid = $1', [id]);
    return data;
};

export const updateKolo = async (rez, id) => {
    const data = await client.query('UPDATE kolo SET pobjednik = $1 WHERE koloid = $2', [rez, id]);
    return data;
};

export const getNatjecatelji = async (id) => {
    const data = await client.query('SELECT * FROM natjecatelji WHERE natjecanjeid = $1 order by bodovi desc, natjecateljname', [id]);
    return data;
};

export const getNatjecateljiID = async (id) => {
    const data = await client.query('SELECT * FROM natjecatelji WHERE natjecateljid = $1', [id]);
    return data.rows[0];
};

export const updateNatjecatelja = async (rez, id) => {
    const data = await client.query('UPDATE natjecatelji SET bodovi = $1 WHERE natjecateljid = $2', [rez, id]);
    return data;
};
